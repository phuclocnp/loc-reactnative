import React from 'react';
import { StackNavigator } from 'react-navigation';
import HomePage from './components/HomePage';

const App = StackNavigator({
  Home: { screen: HomePage }
})

export default App;
