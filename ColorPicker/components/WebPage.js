import React from 'react'
import { WebView, StyleSheet } from 'react-native'

const WebPage = ({ navigation }) => (
  <WebView style={styles.container} 
    source={navigation.state.params} />
)

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})

WebPage.navigationOptions = {
  title: 'All Colors'
}

export default WebPage