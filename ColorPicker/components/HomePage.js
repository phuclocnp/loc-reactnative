import React, { Component } from 'react';
import {
  View,
  Text,
  ImageBackground,
  Slider,
  Dimensions,
  TouchableHighlight,
  StyleSheet
} from 'react-native';
import ColorTools from 'color';
import LinearGradient from 'react-native-linear-gradient';
import ToastNative from '../native-modules/Toast';
import Camera from '../native-modules/Camera';

class HomePage extends Component {
  static navigationOptions = {
    headerStyle: {
      display: 'none'
    }
  };
  constructor() {
    super();
    this.state = {
      hValue: 0,
      sValue: 100,
      lValue: 50
    }
    this.onSliderChange = this.onSliderChange.bind(this);
    this.openCamera = this.openCamera.bind(this);
  }

  rgbToHex(r, g, b) {
    return '#' + [r, g, b].map(x => {
      const hex = x.toString(16)
      return hex.length === 1 ? '0' + hex : hex
    }).join('')
  }

  onSliderChange(value, type) {
    switch (type) {
      case 'h':
        this.setState({hValue: Math.round(value * 360)})
        break;
      case 's':
        this.setState({sValue: Math.round(value * 100)})
        break;
      case 'l':
        this.setState({lValue: Math.round(value * 100)})
        break;
      default:
        break;
    }
  }

  openCamera() {
    Camera.openCamera();
  }

  render() {
    const color = ColorTools(
      `hsl(${this.state.hValue},${this.state.sValue}%,${this.state.lValue}%)`
    );
    const rootColors = [
      ColorTools('hsl(0,100%,50%)'),
      ColorTools('hsl(45,100%,50%)'),
      ColorTools('hsl(90,100%,50%)'),
      ColorTools('hsl(135,100%,50%)'),
      ColorTools('hsl(180,100%,50%)'),
      ColorTools('hsl(225,100%,50%)'),
      ColorTools('hsl(270,100%,50%)'),
      ColorTools('hsl(315,100%,50%)'),
      ColorTools('hsl(360,100%,50%)')
    ]
    return (
      <View style={styles.container}>
        <View styles={styles.container}>
          <Text style={styles.title}>Color Picker</Text>
          <View style={[styles.result,{backgroundColor: color}]}>
          </View>
        </View>
        <View style={styles.container}>
          <View style={styles.row}>
            <LinearGradient
              colors={rootColors}
              locations={[0,0.125,0.25,0.347,0.5,0.625,0.75,0.825,1]}
              start={{ x: 0, y: 1 }}
              end={{ x: 1, y: 1 }}
              style={styles.linearGradient}>
              <Slider
                value={this.state.hValue/360}
                thumbTintColor={color.negate()}
                style={styles.slider}
                onValueChange={value => this.onSliderChange(value, 'h')}
              />
            </LinearGradient>
            <Text style={styles.label}>H: {this.state.hValue}</Text>
          </View>
          <View style={styles.row}>
            <LinearGradient
              colors={[
                ColorTools(`hsl(${this.state.hValue},0%,50%)`),
                ColorTools(`hsl(${this.state.hValue},50%,50%)`),
                ColorTools(`hsl(${this.state.hValue},100%,50%)`)
              ]}            
              start={{ x: 0, y: 1 }}
              end={{ x: 1, y: 1 }}
              style={styles.linearGradient}>
              <Slider
                value={this.state.sValue/100}
                thumbTintColor={color.negate()}
                style={styles.slider}
                onValueChange={value => this.onSliderChange(value, 's')}
              />
            </LinearGradient>
            <Text style={styles.label}>S: {this.state.sValue}%</Text>
          </View>
          <View style={styles.row}>
            <LinearGradient
              colors={[
                ColorTools(`hsl(${this.state.hValue},100%,0%)`),
                ColorTools(`hsl(${this.state.hValue},100%,50%)`),
                ColorTools(`hsl(${this.state.hValue},100%,100%)`)
              ]}
              start={{ x: 0, y: 1 }}
              end={{ x: 1, y: 1 }}
              style={styles.linearGradient}>
              <Slider
                value={this.state.lValue/100}
                thumbTintColor={color.negate()}
                style={styles.slider}
                onValueChange={value => this.onSliderChange(value, 'l')}
              />
            </LinearGradient>
            <Text style={styles.label}>L: {this.state.lValue}%</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.text}>{color.rgb().string()}</Text>
            <Text style={styles.text}>{`#${color.rgbNumber().toString(16)}`}</Text>
          </View>
        </View>
        <TouchableHighlight style={styles.button}
          onPress={() => this.openCamera()}
          underlayColor='#ffb3bf' >
          <Text>Take photo</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const Window = Dimensions.get('window');
const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  slider: {
    flex: 1
  },
  linearGradient: {
    flex: 5,
    height: 40,
    borderRadius: 20
  },
  row: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  text: {
    color: 'black',
    flex: 1,
    textAlign: 'center'
  },
  label: {
    width: 60,
    marginLeft: 10,
    color: 'black'
  },
  title: {
    color: 'black',
    fontSize: 30,
    textAlign: 'center'
  },
  result: {
    width: 200,
    height: 200,
    margin: 20,
    borderRadius: 100
  },
  button: {
    backgroundColor: 'pink',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 160,
    height: 40
  }
};

export default HomePage;
